<?php 

class Building {
    protected $name;
    protected $floors;
    protected $location;

    public function __construct($name, $floors, $location) {
    $this->name = $name;
    $this->floors = $floors;
    $this->location = $location;
    }

    public function getName() {
        return $this->name;
    }

    public function getFloors() {
        return $this->floors;
    }

    public function getLocation() {
        return $this->location;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }
}

class Condominium extends Building {}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium ('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');