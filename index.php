<?php require_once "./code.php "?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S4</title>
</head>
<body>
    <h1>Building</h1>
    <p>The name of the building is <?= $building->getName() ?>. </p>
    <p>The <?= $building->getName() ?> has <?= $building->getFloors() ?> floors.</p>
    <p>The <?= $building->getName() ?> is located at <?= $building->getLocation() ?>.</p>
    <p>The name of the building has been changed to <?= $building->setName('Caswynn Complex')->getName() ?>.</p>

    <h1>Condominium</h1>
    <p>The name of the condominium is <?= $condominium->getName() ?>. </p>
    <p>The <?= $condominium->getName() ?> has <?= $condominium->getFloors() ?> floors.</p>
    <p>The <?= $condominium->getName() ?> is located at <?= $condominium->getLocation() ?>.</p>
    <p>The name of the condominium has been changed to <?= $condominium->setName('Enzo Towers')->getName() ?>.</p>
    


</body>
</html>